Symfony Demo Application
========================

Run
---

```bash
$ php bin/console server:run
```


Liskov Substitution Principle
-----------------------------

**1. task**
examine page: http://127.0.0.1:8000/lsp/form and http://127.0.0.1:8000/lsp/form2
find what is wrong ( hint: https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/cache.html )
fix this
